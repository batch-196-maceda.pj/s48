import {useState,useEffect} from 'react'
import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';


export default function Courses(){
// console.log(coursesData);
// console.log(coursesData[0]);

// //creating a new variable for use in this case courses
// //courseData.map-> loops to ever member of the array. 
// //corse=> is the recieving end of the mentioned commanc above
// const courses = coursesData.map(course=>{
// //return is the command taht dictates what to return in this case Create a course card in every member of the array.(courseData)
// //key is the unique identifier for ever item on the array key
// //while courseprop serves as the recieving variable for the procesed datas.
// 	return(
// 		<CourseCard key={course.id}courseProp = {course}/>
// 		)
//})

	const [courses, setCourses] = useState([]);

	useEffect(()=> {
		fetch('http://localhost:4000/courses')
		.then(res=> res.json())
		.then(data=>{
			console.log(data);

			setCourses(data.map(course=>{
				return(
					<CourseCard key={course._id}courseProp = {course}/>
					)
			}))
		})

	},[]);
	return(
		<>
		<h1>Available Courses:</h1>
		{courses}
		</>
		)
}
import{Navigate} from 'react-router-dom';
import{useContext,useEffect} from 'react';
import UserContext from '../UserContext';


export default function Logout(){

	const{unsetUser,setUser}=useContext(UserContext);
	//clearing the local storage by invoking the unset function
	unsetUser();
	// localStorage.clear() - Terminated because prover is present.
	

	//adding use effect this will allow the logout page to render first before trigering the use effevt which changes the userstate!
	useEffect(()=>{

			//this is the new set up upon log out!
		setUser({id: null})
	})

	return(
		<Navigate to="/login"/> 
		)
};
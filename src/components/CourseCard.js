import{useState} from 'react';
import{Row,Col,Card,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
	//see if it was successfully submitted
	//console.log(props);
	//making sure the type of variable
	//console.log(typeof props);
//destructuring of the OBJ
const{name,description,price,_id} =courseProp
// /*react hooks -userState -> store its state
// Syntax: const [getter, setter] =usetate(initialgetterValue);*/
// const[count, setCount] = useState(0);
// console.log(useState(0));

// function enroll(){
// 	setCount(count +1);
// 	if(count ===10){
// 		alert("No seats left!") 
// 	}else{
// 		return (`${count}`)
// 	}
// 	console.log(`Enrollees: ${count}`)
// }
// const[less, seatCount] = useState(10);
// console.log(useState(10));

// function seatReservation(){
// 	seatCount(less -1);
// 	if(less ===0){
// 		alert ("No more seats left! COme try again next month!")
// 	}else{
// 		alert(`The seats availabe is ${less}`)
// 	}
// 	console.log(`Enrollees: ${less}`)

// }
	return(
	<Row>
			<Col xs={12} md={12}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						{name}
					</Card.Title>
					<Card.Subtitle>
						Description:
					</Card.Subtitle>
						<Card.Text>
						{description}
						</Card.Text>
					
					<Card.Subtitle>
					Price: 
					</Card.Subtitle>
					<Card.Text>
					{price}
					</Card.Text>

					<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
					</Card.Body>
			</Card>

			</Col>
	</Row>

		)
};